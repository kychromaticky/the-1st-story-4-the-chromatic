import React from 'react';

import { Avatar } from './Avatar'; 
import logo from './logo.svg';

 
export default {
  title: 'Avatar ',
  component: Avatar,
  argTypes: {
    backgroundColor: { control: 'color' },
  }, 
}; 

const Template = (args) => <Avatar {...args} />;

export const FirstStory = Template.bind({});

FirstStory.args = {
  /* the args you need here will depend on your component */
 
    primary: true,
    label: 'Der 1st Avatar',
    logo: logo
 
};

